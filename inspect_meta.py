import numpy as np
import argparse
import imutils
import time
import cv2
import os
import glob
from scipy.spatial import distance
import math
from sort import *


def start():
    # construct the argument parse and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--input",
        help="path to input video", default='input/idp_sample.mp4')
    ap.add_argument("-o", "--output",
        help="path to output video", default='output/highway.mp4')
    ap.add_argument("-y", "--yolo", 
        help="base path to YOLO directory", default='yolo-coco')
    ap.add_argument("-c", "--confidence", type=float, default=0.5,
        help="minimum probability to filter weak detections")
    ap.add_argument("-t", "--threshold", type=float, default=0.3,
        help="threshold when applyong non-maxima suppression")
    args = vars(ap.parse_args())


    line = [(210, 242), (210, 542)]
    _init(args, line)



# Return true if line segments AB and CD intersect
def intersect(A,B,C,D):
	return ccw(A,C,D) != ccw(B,C,D) and ccw(A,B,C) != ccw(A,B,D)

def ccw(A,B,C):
	return (C[1]-A[1]) * (B[0]-A[0]) > (B[1]-A[1]) * (C[0]-A[0])


def YOLO_init(args):
    # derive the paths to the YOLO weights and model configuration
    weightsPath = os.path.sep.join([args["yolo"], "yolov3.weights"])
    configPath = os.path.sep.join([args["yolo"], "yolov3.cfg"])
    net = cv2.dnn.readNetFromDarknet(configPath, weightsPath)
    ln = net.getLayerNames()
    ln = [ln[i[0] - 1] for i in net.getUnconnectedOutLayers()]
    return net, ln


def _init(args, line):
    tracker = Sort()
    memory = {}    
    counter = 0
    # load the COCO class labels our YOLO model was trained on
    #labelsPath = os.path.sep.join([args["yolo"], "coco.names"])

    # initialize a list of colors to represent each possible class label
    np.random.seed(42)
    COLORS = np.random.randint(0, 255, size=(200, 3),
        dtype="uint8")

    net, ln = YOLO_init(args)


    vs = cv2.VideoCapture(args["input"])
    (W, H) = (None, None)

    frameIndex = 0

    ################# 
    # FPS Calculation from Video File
    try:
        prop = cv2.cv.CV_CAP_PROP_FRAME_COUNT if imutils.is_cv2() \
            else cv2.CAP_PROP_FRAME_COUNT
        total = int(vs.get(prop))
        print("[INFO] {} total frames in video".format(total))
    except:
        print("[INFO] could not determine # of frames in video")
        print("[INFO] no approx. completion time can be provided")
        total = -1
    ##################


    # loop over frames from the video file stream
    print('Calculating FPS....')
    startFPS = time.time()
    frameIndex = 0
    for i in range(100):
        (grabbed, frame) = vs.read()
        if not grabbed:
            break

        frameIndex += 1
            

    endFPS = time.time()
    seconds = endFPS - startFPS
    fps  = frameIndex / seconds
    print("Estimated FPS : {}".format(fps))

    print('Displaying stream....')
    while True:
        (grabbed, frame) = vs.read()
        if not grabbed:
            break

        scale_percent = 60 # percent of original size
        width = int(frame.shape[1] * scale_percent / 100)
        height = int(frame.shape[0] * scale_percent / 100)
        dim = (width, height)
        # resize image
        frame = cv2.resize(frame, dim, interpolation = cv2.INTER_AREA)
        
        cv2.imshow('Stream',frame)

        #cv2.waitKey(30)
        if cv2.waitKey(1) == 27:
            break

    # release the file pointers
    print("Streaming stopped....")

    #line = [(210, 242), (210, 542)]
    print("line co-ordinates: ")
    x1 = input("x1: ")
    y1 = input("y1: ")
    x2 = input("x2: ")
    y2 = input("y2: ")
    line = [(int(x1), int(y1)), (int(x2), int(y2))]

    while True:
        (grabbed, frame) = vs.read()
        if not grabbed:
            break

        scale_percent = 60 # percent of original size
        width = int(frame.shape[1] * scale_percent / 100)
        height = int(frame.shape[0] * scale_percent / 100)
        dim = (width, height)
        # resize image
        frame = cv2.resize(frame, dim, interpolation = cv2.INTER_AREA)
        # draw line
        cv2.line(frame, line[0], line[1], (0, 255, 255), 5)
        cv2.imshow('image',frame)
        if cv2.waitKey(0) == 27:
            break

    vs.release()





if __name__ == '__main__':
    start()